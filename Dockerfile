FROM php:7.4-fpm-alpine

ARG AMQP_VERSION
ARG COMPOSER_VERSION
ARG IGBINARY_VERSION
ARG REDIS_VERSION

# Install dependencies
RUN apk add --no-cache \
    icu-libs \
    libpq \
    libzip \
    rabbitmq-c \
    supervisor \
    zstd-dev

# Install PHP extensions
RUN set -eux \
    && apk add --no-cache --virtual .build-deps \
        autoconf \
        g++ \
        icu-dev \
        libzip-dev \
        rabbitmq-c-dev \
        make \
        postgresql-dev \
    && docker-php-source extract \
    && docker-php-ext-install -j$(nproc) \
        intl \
        opcache \
        pdo_mysql \
        pdo_pgsql \
        pgsql \
        zip \
    && cd /usr/src/php/ext \
    && pecl bundle amqp-${AMQP_VERSION} \
    && docker-php-ext-install -j$(nproc) amqp \
    && pecl bundle igbinary-${IGBINARY_VERSION} \
    && docker-php-ext-install -j$(nproc) igbinary \
    && pecl bundle redis-${REDIS_VERSION} \
    && docker-php-ext-configure redis --enable-redis-igbinary=y --enable-redis-zstd=y \
    && docker-php-ext-install -j$(nproc) redis \
    && docker-php-source delete \
    && apk del .build-deps

# add main supervisor config
COPY provisioning/supervisor/supervisord.conf /etc/supervisord.conf

# Install Composer
RUN set -eux \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=${COMPOSER_VERSION}
